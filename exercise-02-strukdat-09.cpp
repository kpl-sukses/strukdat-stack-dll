/************************************************
Nama Program    : exercise-02.cpp
Nama            : Mochamad Arya Bima Agfian
NPM             : 140810190031
Tanggal Buat    : 3 Mei 2020
Deskripsi       : Queue and dequeue reversal.
*************************************************/
#include <iostream>
#include <windows.h>
#include <string.h>

using namespace std;

struct element  //struct elemen queue
{
    char word[20];
    element* next;
};

struct queue    //struct queue nya
{
    element* front;
    element* back;
};
queue Q,Qr;     //deklarasi global

typedef element* pointer;   //typedef untuk pointer element

void menu();    //ya menu
void createQueue(queue& Q);     //membuat antrian/queue
void createElement(pointer& p_new);     //membuat element untuk queue/antrian
bool isEmpty(queue Q);      //mengecek kekosongan antrian/queue
string rFront(queue Q);     //mengembalikan/return elemen pertama queue jika ada
void enqueue(queue& Q, pointer p_new);      //memasukkan sebuah elemen ke dalam queue
void dequeue(queue& Q, pointer& p_del);      //membuang sebuah elemen dari queue
void reversal(queue& Q);     //membalikkan queue
void traversal(queue Q);      //print semua yang ada dalam queue

main()
{
    pointer p_new, p_del;      //remnant buat nyimpan data yang dihapus
    int pick;                           //buat switch
    bool breaker = true;                //buat stop looping
    createQueue(Q);
    do
    {
        menu();
        cin >> pick;
        switch(pick)
        {
            case 1:
            system("cls");
            createElement(p_new);
            enqueue(Q, p_new);
            break;
            case 2:
            system("cls");
            dequeue(Q, p_del);      //walaupun pass by reference, p_del ngga kepake lagi di main
            break;
            case 3:
            system("cls");
            if(isEmpty(Q) == true)
                cout << "\nQueue is empty\n";
            break;
            case 4:
            system("cls");
            if(isEmpty(Q) == true)              //pastiin queue nya gak kosong
                cout << "\nQueue is empty\n";
            else                                //karena queue nya gak kosong, bisa jalan functionnnya
                cout << rFront(Q) << endl;
            break;
            case 5:
            system("cls");
            traversal(Q);                   //udah ada fungsi reversal di dalemnya
            break;
            case 6:
            cout << "\nBye\n";
            breaker = false;
            break;
            default:
            system("cls");
            cout << "\nInput error, please kindly reinput\n";
        }
    } while(breaker == true);
    return 0;
}

void menu()     //menu ya menu
{
    cout << "\nQueue\n\n";
    cout << "1. Input an element to queue\n2. Remove an element from queue\n";
    cout << "3. Check whether queue is empty\n4. Show the first word in the queue\n";
    cout << "5. Print queue from the back withouth changing the queue\n6. Exit\n";
    cout << "Your pick : ";
}
void createQueue(queue& Q)      //bikin queue
{
    Q.front = NULL;
    Q.back = NULL;
}
void createElement(pointer& p_new)      //bikin elemen baru
{
    p_new = new element;
    cout << "Input a word without space as stored data (max. 20): ";
    cin >> p_new->word;
    p_new->next = NULL;
}
bool isEmpty(queue Q)
{
    if(Q.front == NULL)
        return true;        //kalau beneran kosong, return true
    else
        return false;       //kalau tidak kosong, return false
}
string rFront(queue Q)
{
    return Q.front->word;   //kalau ada isinya, bakal direturn
}
void enqueue(queue& Q, pointer p_new)
{
    if(Q.front == NULL)     //kasus isinya kosong
    {
        Q.front = p_new;
        Q.back = p_new;
    }
    else                    //kasus isinya gak kosong
    {
        Q.back->next = p_new;
        Q.back = p_new;
    }
}
void dequeue(queue& Q, pointer& p_del)
{
    if(Q.front == NULL)       //kasus isinya kosong
        cout << "\nQueue is empty, nothing to delete\n";
    else                      //kasus isinya gak kosong
    {
        p_del = Q.front;
        Q.front = Q.front->next;
        p_del->next = NULL;
    }
}
void reversal(queue& Q)
{
    pointer remnant;
    dequeue(Q, remnant);    //menghapus sebuah element dan disimpan di remnant
    if(Q.front != NULL)     //menjalankan rekursif selama pointer tidak NULL
        reversal(Q);        //rekursif untuk menghapus semua isi dulu, baru dimasukkan kembali
    enqueue(Q, remnant);    //memasukkan kembali fungsi ke queue
}
void traversal(queue Q)   //print dong
{
    if(Q.front == NULL)     //kasus kosong
        cout << "\nQueue is empty, nothing to print\n";
    else
    {
        if(Q.front->next == NULL)   //kasus isi 1, aku bikin biar gak berantakan di reversal
            cout << "Queue [" << Q.front->word << "]\n";
        else                        //kalau kasusnya lebih dari 1, bakal ngejalanin reversal dulu
        {
            reversal(Q);            //yang memutar balikkan queue
            pointer trav = Q.front;    //ini yang jalan jalan, dia pointer ke Qr karena Qr yang udah sorted
            cout << "\nQueue [";
            while (trav != NULL)
            {
                cout << trav->word;
                if(trav->next != NULL)
                    cout << " , ";
                trav = trav->next;
            }
            cout << "]\n";
        }
    }
}