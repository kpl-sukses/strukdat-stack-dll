/************************************************
Nama Program    : exercise-01.cpp
Nama            : Mochamad Arya Bima Agfian
NPM             : 140810190031
Tanggal Buat    : 2 Mei 2020
Deskripsi       : Queue and priority list.
*************************************************/
#include <iostream>
#include <windows.h>
#include <string.h>

using namespace std;

int count=0;
struct element
{
    char word[20];
    int priority;
    element* next;
};

struct queue
{
    element* front;
    element* back;
};
queue Q,Qr;

typedef element* pointer;

void menu();
void createQueue(queue& Q);
void createElement(pointer& p_new, int& count);
bool isEmpty(queue Q);
string rFront(queue Q);
void swap(char a[], char b[]);
void enqueue(queue& Q, pointer p_new);
void dequeue(queue& Q, pointer p_del, int& count);
void prioriter(queue Q, queue& Qr, int count);
void traversal(queue Q, queue& Qr, int count);

main()
{
    pointer p_new, p_del;
    int pick;
    bool breaker = true;
    createQueue(Q);
    do
    {
        menu();
        cin >> pick;
        switch(pick)
        {
            case 1:
            system("cls");
            createElement(p_new, count);
            enqueue(Q, p_new);
            break;
            case 2:
            system("cls");
            dequeue(Q, p_del, count);
            break;
            case 3:
            system("cls");
            if(isEmpty(Q) == true)
                cout << "\nQueue is empty\n";
            break;
            case 4:
            system("cls");
            if(isEmpty(Q) == true)
                cout << "\nQueue is empty\n";
            else
                cout << rFront(Q) << endl;
            break;
            case 5:
            system("cls");
            traversal(Q, Qr, count);
            break;
            case 6:
            cout << "\nBye\n";
            breaker = false;
            break;
            default:
            system("cls");
            cout << "\nInput error, please kindly reinput\n";
        }
    } while(breaker == true);
    return 0;
}

void menu()
{
    cout << "\nPriority List Queue\n";
    cout << "1. Input an element to queue\n2. Remove an element from queue\n";
    cout << "3. Check whether queue is empty\n4. Show the first word in the queue\n";
    cout << "5. Print queue based on priority\n6. Exit\n";
    cout << "Your pick : ";
}
void createQueue(queue& Q)
{
    Q.front = NULL;
    Q.back = NULL;
}
void createElement(pointer& p_new, int& count)
{
    p_new = new element;
    cout << "Input a word without space as stored data (max. 20): ";
    cin >> p_new->word;
    cout << "Input priority number, higher number will be prioritised : ";
    cin >> p_new->priority;
    cout << "If the priority number is similar, data in front of queue will be printed first\n";
    p_new->next = NULL;
    count++;
}
bool isEmpty(queue Q)
{
    if(Q.front == NULL)
        return true;
    else
        return false;
}
string rFront(queue Q)
{
    return Q.front->word;
}
void swap(char a[], char b[])
{
    char tmp[20];
    strcpy(tmp, a);
    strcpy(a, b);
    strcpy(b,tmp);
}
void enqueue(queue& Q, pointer p_new)
{
    if(Q.front == NULL)
    {
        Q.front = p_new;
        Q.back = p_new;
    }
    else
    {
        Q.back->next = p_new;
        Q.back = p_new;
    }
}
void dequeue(queue& Q, pointer p_del, int& count)
{
    if(Q.front == NULL)
        cout << "\nQueue is empty, nothing to delete\n";
    else
    {
        p_del = Q.front;
        Q.front = Q.front->next;
        p_del->next = NULL;
    }
    count--;
}
void prioriter(queue Q, queue& Qr, int count)
{
    createQueue(Qr);
    queue arr[count];
    for (int i=0; i<count; i++)
    {
        arr[i].front = Q.front;
        Q.front = Q.front->next;
    }

    for(int k=0; k<count-1; k++)
        for(int j=0; j<count-k-1; j++)
        {
            if(arr[j].front->priority < arr[j+1].front->priority)
            {
                swap(arr[j].front->word, arr[j+1].front->word);
                cout << "Switched " << arr[j].front->word << " with " << arr[j+1].front->word << endl;
            }
            else
                cout << "No change" << endl;
        }
    for(int z=0; z<count; z++)
    {
        enqueue(Qr, arr[z].front);
    }
}
void traversal(queue Q, queue& Qr, int count)
{
    if(Q.front == NULL)
        cout << "\nQueue is empty, nothing to print\n";
    else
    {
        if(Q.front->next == NULL)
            cout << "Queue [" << Q.front->word << "]\n";
        else
        {
        prioriter(Q, Qr, count);
        pointer trav = Qr.front;
        cout << "\nQueue [";
        while (trav != NULL)
        {
            cout << trav->word;
            if(trav->next != NULL)
                cout << " , ";
            trav = trav->next;
        }
        cout << "]\n";
        }
    }
}