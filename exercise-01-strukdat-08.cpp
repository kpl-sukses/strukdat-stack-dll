/************************************************
Nama Program    : exercise-01.cpp
Nama            : Mochamad Arya Bima Agfian
NPM             : 140810190031
Tanggal Buat    : 24 April 2020
Deskripsi       : Stack.
*************************************************/
#include <iostream>
#include <windows.h>
using namespace std;

struct kartu
{
    char simbol[3];
    char angka[3];
};

struct tumpukan
{
    kartu card;
    tumpukan* next;
};

typedef tumpukan* pointer;
typedef pointer S;

void menu();                            //Menu
void createStack(S& top);               //Membuat list stack
void createElement(pointer& p_new);     //Membuat element untuk stack
void push(S& top, pointer p_new);       //Push function untuk stack (insert last)
void pop(S& top, pointer p_del);        //Pop function untuk stack (delete last)
void peek(S top);                       //Mengoutput isi top jika ada
bool isEmpty(S top);                    //Mengecek apakah top ada/kosong
void printCards(S top);                 //Mengoutput semua kartu

int main()
{
    S stack;
    pointer p_new, p_del;
    char pick;
    bool breaker = true;
    createStack(stack);
    do
    {
        menu();
        cout << "\nYour pick : "; cin >> pick;
        switch(pick)
        {
            case '1':
            system("cls");
            createElement(p_new);
            push(stack, p_new);
            break;
            case '2':
            system("cls");
            pop(stack, p_del);
            break;
            case '3':
            system("cls");
            peek(stack);
            break;
            case '4':
            isEmpty(stack);
            system("cls");
            if(isEmpty(stack) == true)
                cout << "Deck is empty!\n\n";
            else
                cout << "Deck is filled\n\n";
            break;
            case '5':
            system("cls");
            printCards(stack);
            break;
            case '6':
            cout << "\nBye.";
            breaker = false;
            break;
            default:
            cout << "\nInput is incorrect, please kindly reinput\n";
        }
    } while (breaker == true);
}

void menu()
{
    cout << "Card Stack Deck, use any character for 'Symbol', use only numbers for number\n";
    cout << "1. Push a new element\n2. Pop the last element\n";
    cout << "3. Peek the top card\n4. Check whether deck is empty\n";
    cout << "5. Print Cards\n6. Quit Program\n";
}
void createStack(S& top)
{
    top = NULL;
}
void createElement(pointer& p_new)
{
    p_new = new tumpukan;
    cout << "Input symbol for this card (2 characters max.) : "; cin >> p_new->card.simbol;
    cout << "Input number for this card (2 numbers max.) : "; cin >> p_new->card.angka;
    cin.ignore();
    p_new->next = NULL;
}
void push(S& top, pointer p_new)
{
    if(top == NULL)
        top = p_new;
    else
    {
        p_new->next = top;
        top = p_new;
    }
}
void pop(S& top, pointer p_del)
{
    if(top == NULL)
        cout << "Deck is Empty, nothing to pop\n\n";
    else if(top->next == NULL)
    {
        cout << "Last element in the Deck is deleted.\n\n";
        p_del = top;
        top = NULL;
    }
    else
    {
        cout << "Last element in the Deck is deleted.\n\n";
        p_del = top;
        top = top->next;
        p_del->next = NULL;
    }
}
void peek(S top)
{
    if(top == NULL)
        cout << "This deck is empty\n\n";
    else
        cout << "First card is " << top->card.simbol << " " << top->card.angka << endl << endl;
}
bool isEmpty(S top)
{
    if(top == NULL)
        return true;
    else
        return false;
}
void printCards(S top)
{
    int i=1;
    if(top == NULL)
        cout << "This deck is empty\n\n";
    else
    {
    pointer tmp = top;
    cout << "No.\tSymbol\tNumber\n";
    cout << "------------------------------\n";
    while(tmp != NULL)
    {
        cout << i++ << "\t" << tmp->card.simbol << "\t" << tmp->card.angka << endl; 
        tmp = tmp->next;
    }
    cout << "------------------------------\n\n";
    }
}