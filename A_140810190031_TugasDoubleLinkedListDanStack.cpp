/************************************************
Nama Program    : A_140810190031_TugasDoubleLinkedListDanStack.cpp
Nama            : Mochamad Arya Bima Agfian
NPM             : 140810190031
Tanggal Buat    : 12 April 2020
Deskripsi       : Doubly linked list and Stack.
*************************************************/
#include <iostream>
#include <windows.h>

using namespace std;

struct stack
{
    int num;
    stack* next_stack;
};

struct Elementlist
{
    int id;
    char name[30];
    Elementlist* next;
    Elementlist* prev;
};

typedef stack* pointer_stack;
typedef Elementlist* pointer_Elementlist;
typedef pointer_stack tumpukan;
typedef pointer_Elementlist List;

void menu();
void menuStack();
void createStack(tumpukan& top);
void createElementStack(pointer_stack& p_new);
void push(tumpukan& top, pointer_stack p_new);
void pop(tumpukan& top, pointer_stack p_del);
void traversalStack(tumpukan top);
void menuList();
void createList(List& head);
void createElementList(pointer_Elementlist& p_new);
void insertFirst(List& head, pointer_Elementlist p_new);
void insertAfterByKey(List& head, pointer_Elementlist p_new, pointer_Elementlist& p_search, int& key, bool status);
void insertBeforeByKey(List& head, pointer_Elementlist p_new, pointer_Elementlist& p_search, int& key, bool status);
void insertLast(List& head, pointer_Elementlist p_new);
void deleteFirst(List& head, pointer_Elementlist p_del);
void deleteByKey(List& head, pointer_Elementlist p_del, pointer_Elementlist& p_search, int& key, bool status);
void deleteLast(List& head, pointer_Elementlist p_del);
void searchElement(List& head, int& key, pointer_Elementlist& p_search, bool& status);
void traversalList(List head);

main()
{
    tumpukan lottery;
    List champion;
    pointer_stack new_lottery, del_lottery;
    pointer_Elementlist new_champ, del_champ, search_champ;
    bool breaker = true;
    bool status = true;
    int key;
    char confirmation,pick;
    createStack(lottery);
    createList(champion);
    cout << "Welcome, please enjoy this program :)\n";
    do
    {
        menu();
        cout << "\nPick : ";
        cin >> pick;
        switch(pick)
        {
            case '1':
            system("cls");
            cout << "Continue with Stack? Input 'Y' or 'y' to continue: "; cin >> confirmation;
            if(confirmation == 'y' || confirmation == 'Y')
            {
                createStack(lottery);
                do
                {
                    cout << endl;
                    menuStack();
                    cout << "\nPick : ";
                    cin >> pick;
                    switch(pick)
                    {
                        case '1':
                        system("cls");
                        createElementStack(new_lottery);
                        push(lottery, new_lottery);
                        break;
                        case '2':
                        pop(lottery, del_lottery);
                        break;
                        case '3':
                        system("cls");
                        traversalStack(lottery);
                        break;
                        case '4':
                        cout << "Thank you for using the program, stay safe under pandemic :)\n";
                        breaker = false;
                        break;
                        default:
                        cout << "\nInput error, please kindly reinput\n";
                    }
                } while (breaker == true);
            }
            break;
            case '2':
            system("cls");
            cout << "Continue with List? Input 'Y' or 'y' to continue: "; cin >> confirmation;
            if(confirmation == 'y' || confirmation == 'Y')
            {
                createList(champion);
                do
                {
                   menuList();
                   cout << "\nPick : ";cin >> pick;
                   switch(pick)
                   {
                       case '1':
                       cout << "Continue with Insert? Input 'Y' or 'y' to continue: "; cin >> confirmation;
                       if(confirmation == 'y' || confirmation == 'Y')
                       {
                           bool breaker2 = true;
                           do
                           {
                               cout << "1. First\n2. After a certain ID\n3. Before a certain ID\n4. Last\n5. Cancel Insertion\n";
                               cout << "Pick : ";
                               cin >> pick;
                               switch(pick)
                               {
                                   case '1':
                                   system("cls");
                                   createElementList(new_champ);
                                   insertFirst(champion, new_champ);
                                   breaker2 = false;
                                   break;
                                   case '2':
                                   system("cls");
                                   cout << "\nInsert ID as Key : "; cin >> key;
                                   insertAfterByKey(champion, new_champ, search_champ, key, status);
                                   breaker2 = false;
                                   break;
                                   case '3':
                                   system("cls");
                                   cout << "\nInsert ID as Key : "; cin >> key;
                                   insertBeforeByKey(champion, new_champ, search_champ, key, status);
                                   breaker2 = false;
                                   break;
                                   case '4':
                                   system("cls");
                                   createElementList(new_champ);
                                   insertLast(champion, new_champ);
                                   breaker2 = false;
                                   break;
                                   case '5':
                                   system("cls");
                                   breaker2 = false;
                                   break;
                                   default:
                                   system("cls");
                                   cout << "\nInput error, please kindly reinput\n";
                               }
                           }while (breaker2 == true);
                           breaker2 = true;
                       }
                       break;
                       case '2':
                       cout << "Continue with Delete? Input 'Y' or 'y' to continue : "; cin >> confirmation;
                       if(confirmation == 'y' || confirmation == 'Y')
                       {
                           bool breaker2 = true;
                           do
                           {
                               cout << "1. First\n2. A Certain ID\n3. Last\n4. Cancel Deletion\n";
                               cout << "Pick : "; cin >> pick;
                               switch(pick)
                               {
                                   case '1':
                                   deleteFirst(champion, del_champ);
                                   breaker2 = false;
                                   break;
                                   case '2':
                                   cout << "\nInsert ID as Key : "; cin >> key;
                                   deleteByKey(champion,del_champ, search_champ, key, status);
                                   breaker2 = false;
                                   break;
                                   case '3':
                                   deleteLast(champion, del_champ);
                                   breaker2 = false;
                                   break;
                                   case '4':
                                   system("cls");
                                   breaker2 = false;
                                   break;
                                   default:
                                   system("cls");
                                   cout << "\nInput error, please kindly reinput\n";
                               }
                           }while(breaker2 == true);
                           breaker2 = true;
                       }
                       break;
                       case '3':
                       traversalList(champion);
                       break;
                       case '4':
                       cout << "Thank you for using the program, stay safe under pandemic :)\n";
                       breaker = false;
                       break;
                       default:
                       system("cls");
                       cout << "\nInput error, please kindly reinput\n";
                   }
                } while (breaker == true);
            }
            break;
            default:
            system("cls");
            cout << "\nInput error, please kindly reinput\n";
        }
    } while (breaker == true);
}

void menu()
{
    cout << "Choose a function for this program\n1. Stack\n2. Linked List\n";
}
void menuStack()
{
    cout << "Choose a function for Stack Program\n1. Push\n2. Pop\n3. Print Data\n4. Quit Program\n";
}
void createStack(tumpukan& top)
{
    top = NULL;
}
void createElementStack(pointer_stack& p_new)
{
    p_new = new stack;
    cout << "Input lottery number : "; cin >> p_new->num;
    p_new->next_stack = NULL;
}
void push(tumpukan& top, pointer_stack p_new)
{
    if(top == NULL)
        top = p_new;
    else
    {
        p_new->next_stack = top;
        top = p_new;
    }
}
void pop(tumpukan& top, pointer_stack p_del)
{
    if(top == NULL)
        cout << "Stack is Empty, nothing to pop\n";
    else if(top->next_stack == NULL)
    {
        p_del = top;
        top = NULL;
    }
    else
    {
        p_del = top;
        top = top->next_stack;
        p_del->next_stack = NULL;
    }
}
void traversalStack(tumpukan top)
{
    pointer_stack tmp;
    if(top == NULL)
        cout << "Stack is empty!" << endl;
    else
    {
        tmp = top;
        while(top != NULL)
        {
            cout << top->num << endl;
            top = top->next_stack;
        }
    }
}
void menuList()
{
    cout << "Choose a function for Linked List Program\n";
    cout << "1. Insert\n2. Delete\n3. Print Data\n4. Quit Program\n";
}
void createList(List& head)
{
    head = NULL;
}
void createElementList(pointer_Elementlist& p_new)
{
    p_new = new Elementlist;
    cout << "ID(Should be Unique Data) : "; cin >> p_new->id;
    cin.ignore();
    cout << "Name (30 characters max.) : "; cin >> p_new->name;
    p_new->next = NULL;
    p_new->prev = NULL;
}
void searchElement(List& head, int& key, pointer_Elementlist& p_search, bool& status)
{
    p_search = head;
    while(p_search != NULL)
    {
        if(p_search->id == key)
        {
            status = false;
            break;
        }
        else
        {
            p_search = p_search->next;
            status = true;
        }
    }
}
void insertFirst(List& head, pointer_Elementlist p_new)
{
    if(head == NULL)
        head = p_new;
    else
    {
        p_new->next = head;
        head = p_new;
    }
}
void insertAfterByKey(List& head, pointer_Elementlist p_new, pointer_Elementlist& p_search, int& key, bool status)
{
    searchElement(head, key, p_search, status);
    if(status == true)
        cout << "Key is not found.\n\n";
    else   
    {
        cout << "Key is found\n";
        createElementList(p_new);
        if(p_search->next == NULL)
        {
            p_search->next = p_new;
            p_new->prev = p_search;
        }
        pointer_Elementlist after = p_search->next;
        p_search->next = p_new;
        p_new->prev = p_search;
        after->prev = p_new;
        p_new->next = after;
    }
}
void insertBeforeByKey(List& head, pointer_Elementlist p_new, pointer_Elementlist& p_search, int& key, bool status)
{
    searchElement(head, key, p_search, status);
    if(status == true)
        cout << "Key is not found.\n\n";
    else   
    {
        cout << "Key is found\n";
        createElementList(p_new);
        if(p_search->prev == NULL)
        {
            p_new->next = head;
            head->prev = p_new;
            head = p_new;
        }
        else
        {
            pointer_Elementlist before = p_search->prev;
            p_search->prev = p_new;
            p_new->next = p_search;
            p_new->prev = before;
            before->next = p_new;
        }
    }
}
void insertLast(List& head, pointer_Elementlist p_new)
{
    List tmp;
    if(head == NULL)
        head = p_new;
    else
    {
        tmp = head;
        while (tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        tmp->next = p_new;
        p_new->prev = tmp;
    }
}
void deleteFirst(List& head, pointer_Elementlist p_del)
{
    if (head == NULL)               //Kasus isi 0 elemen
        p_del = NULL;
    else if(head->next == NULL)     //Kasus isi 1 elemen
    {
        p_del = head;
        head = NULL;
    }
    else                            //Kasus isi > 1 elemen
    {
        p_del = head;
        head = head->next;
        p_del->next = NULL;
    }
}
void deleteByKey(List& head, pointer_Elementlist p_del, pointer_Elementlist& p_search, int& key, bool status)
{
    searchElement(head, key, p_search, status);
    if(status == true)
        cout << "Key is not found.\n";
    else   
    {
        cout << "Key is found, and deleted\n";
        if(p_search->prev == NULL)
        {
            p_del = head;
            head = head->next;
            p_del->next = NULL;
        }
        else if(p_search->next == NULL)
        {
            p_del = p_search;
            p_search->prev->next = NULL;
            p_search->prev = NULL;
        }
        else
        {
            p_del = p_search;
            p_search->prev->next = p_search->next;
            p_search->next->prev = p_search->prev;
            p_search->next = NULL;
            p_search->prev = NULL;
        }
    }
}
void deleteLast(List& head, pointer_Elementlist p_del)
{
    pointer_Elementlist last, prevlast;
    if (head == NULL)               //Kasus isi 0 elemen
        p_del = NULL;
    else if(head->next == NULL)     //Kasus isi 1 elemen
    {
        p_del = head;
        head = NULL;
    }
    else                            //Kasus isi > 1 elemen
    {
        last = head;
        prevlast = NULL;
        while(last->next != NULL)
        {
            prevlast = last;
            last = last->next;
        }
        p_del = last;
        prevlast->next = NULL;
    }
}
void traversalList(List head)
{
    system("cls");
    if(head == NULL)
        cout << "List is Empty!\n";
    else
    {
    cout << "\nID\t\tName\n";
    cout << "----------------------------------------" << endl;
    while(head != NULL)
    {
        cout << head->id << "\t\t" << head->name << endl;
        head = head->next;
    }
    cout << "----------------------------------------" << endl << endl;
    }
}